import datetime

class User:
    def __init__(self,username,password,namauser,umur, role) -> None:
        self.username= username
        self.namauser= namauser
        self.password= password
        self.umur= umur
        self.role= role
    def getUsn(self):
        return self.username
    def getNama(self):
        return self.nama
    def getPass(self):
        return self.password
    def getUmur(self):
        return self.umur
    def getRole(self):
        return self.role

class Kasir(User):
    def __init__(self, username, password, namauser, umur, role):
        super().__init__(username, password, namauser, umur, role)
class Janitor(User):
    def __init__(self, username, password, namauser, umur, role):
        super().__init__(username, password, namauser, umur, role)
class Chef(User):
    def __init__(self, username, password, namauser, umur, role):
        super().__init__(username, password, namauser, umur, role)
daftarUser= {}
def register():
    print("Format data: [username] [password] [nama] [umur] [role]")
    inpuser= str(input("Input data karyawan baru:"))
    splitinp= inpuser.split(" ")
    username= splitinp[0]
    password= splitinp[1]
    namauser= splitinp[2]
    umur= splitinp[3]
    role= splitinp[4]
    print("Karyawan", splitinp[0], "berhasil ditambahkan")

    if role == "kasir" or "Kasir" :
        daftarUser[username] = Kasir(username, password, namauser, umur, role)
    elif role == "janitor" or "Janitor" :
        daftarUser[username] = Janitor(username, password, namauser, umur, role)
    elif role == "chef" or "Chef" :
        daftarUser[username] = Chef(username, password, namauser, umur, role)

while True:
    print("Selamat datang di Sistem Manajemen Homura")
    print("")
    print('''
    Apa yang ingin anda lakukan? (Tulis angka saja)
1. Register karyawan baru
2. Login
8. Status Report
9. Karyawan Report
11. Exit''')
    pilihanuser= input()
    if pilihanuser== "1":
        register()
    if pilihanuser== "11":
        print("TERIMA KASIH TELAH MENGGUNAKAN SISTEM MANAJEMEN HOMURA")
        exit()