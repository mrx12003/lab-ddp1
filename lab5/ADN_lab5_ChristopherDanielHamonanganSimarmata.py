import math
def membuatmatrix(rowCount, colCount, dataList):
    #source:https://stackoverflow.com/questions/40120892/creating-a-matrix-in-python-without-numpy with modification
    mat = []
    index= 0
    for i in range (rowCount):
        rowList = []
        for j in range (colCount):
            rowList.append(int(dataList[index]))
            index+=1
        mat.append(rowList)
    return mat

def penjumlahanmatrix(matA,matB): #source: https://www.geeksforgeeks.org/python-program-add-two-matrices/ with modification
    for i in range(len(matA)):
        for j in range(len(matA[0])):
            hasil= matA[i][j] + matB[i][j]
            return hasil
def penguranganmatrix(matA, matB): #source: seperti pada penjumlahan matriks
    for i in range(len(matA)):
        for j in range(len(matA[0])):
            hasil= matA[i][j] - matB[i][j]
            return hasil
def determinan(matA):
    hasil = matA[0][0]*matA[1][1] - matA[0][1]*matA[1][0]
    return hasil
def transpos(matA): # source:https://www.programiz.com/python-programming/examples/transpose-matrix with modification
    for i in range(len(matA[0])):
        for j in range(len(matA)):
            hasil= matA[j][i]
    return hasil
def ukuranmatriks(ukuran):
    baris = int(ukuran[0])
    kolom = int(ukuran[2])
    listElemen = []
    for i in range(baris):
        elemen = input(("Baris ",str(i+1)," matrix: ")).split(" ")
        if(len(elemen)) != kolom:
            print("Terjadi kesalahan input. Silakan ulang kembali.")
            print()
            break
        else:
            listElemen += elemen
    mat1 = membuatmatrix(baris, kolom, listElemen)
    return mat1

def ukuranmatriks1(ukuran):
    baris = int(ukuran[0])
    kolom = int(ukuran[2])
    listElemen = []
    for i in range(baris):
        elemen = input("Baris ",str(i+1)," matrix: ")
        elemen1= elemen.split(" ")
        if(len(elemen1)) != kolom:
            print("Terjadi kesalahan input. Silakan ulang kembali.")
            print()
            break
        else:
            listElemen += elemen
    mat1 = membuatmatrix(baris, kolom, listElemen)
    return mat1
def ukuranmatriks2(ukuran):
    baris = int(ukuran[0])
    kolom = int(ukuran[2])
    listElemen = []
    for i in range(baris):
        elemen = input("Baris ",str(i+1)," matrix: ")
        elemen1= elemen.split(" ")
        if(len(elemen1)) != kolom:
            print("Terjadi kesalahan input. Silakan ulang kembali.")
            print()
            break
        else:
            listElemen += elemen
    mat1 = membuatmatrix(baris, kolom, listElemen)
    return mat1
def mat2x2():
    baris = 2
    kolom = 2
    listElemen = []
    for i in range(baris):
        elemen = input("Baris ", str(i+1)," matrix: ")
        elemen1= elemen.split(" ")
        if(len(elemen1)) != kolom:
            print("Terjadi kesalahan input. Silakan ulang kembali.")
            print()
            break
        else:
            listElemen += elemen
    mat1 = membuatmatrix(baris, kolom, listElemen)
    return mat1
def printMatrix(matriks): #source: https://java2blog.com/print-a-matrix-in-python/ with modification
    print()
    print("Hasil dari operasi:")
    for baris in matriks:
        for hasil in baris:
            print(hasil, end=" ")
    print('''
    ''')
while True:
    print('''
    Selamat datang di Matrix Calculator. Berikut adalah operasi-operasi yang
    dapat dilakukan:
    1. Penjumlahan
    2. Pengurangan
    3. Transpose
    4. Determinan
    5. Keluar
    ''')

    operasi= input("Silakan pilih operasi:" )
    print('''
    ''')

    if operasi.isdigit():
        if int(operasi) == 1:
            try:
                ukuran= input("Ukuran matriks: ")
                ukuran1= ukuran.split(" ")
                matriks_1= ukuranmatriks1(ukuran1)
                matriks_2= ukuranmatriks2(ukuran1)
                hasil= penjumlahanmatrix(matriks_1,matriks_2)
                printMatrix(hasil)
            except:
                print("Terjadi kesalahan input. Silakan ulang kembali.")
                print()
                continue
        elif int(operasi) == 2:
            try:
                ukuran= input("Ukuran matriks: ")
                ukuran1= ukuran.split(" ")
                matriks_1= ukuranmatriks1(ukuran1)
                matriks_2= ukuranmatriks2(ukuran1)
                hasil= penguranganmatrix(matriks_1,matriks_2)
                printMatrix(hasil)
            except:
                print("Terjadi kesalahan input. Silakan ulang kembali.")
                print()
                continue
        elif int(operasi) == 3: 
            ukuran= input("Ukuran matriks: ").split(" ")
            try:
                matrix = ukuranmatriks(ukuran)
                hasil = transpos(matrix)
                printMatrix(hasil)
            except:
                print("Terjadi kesalahan input. Silakan ulang kembali.")
                print()
                continue
            
        elif int(operasi) == 4:
            try:
                matrix = mat2x2()
                print()
                print("Hasil dari operasi: ")
                print(determinan(matrix))
            except:
                print("Terjadi kesalahan input. Silakan ulang kembali.")
                print()
                continue
        elif int(operasi) == 5: 
            print("Sampai Jumpa!")
            break
    else:
        print("Terjadi kesalahan input")
        